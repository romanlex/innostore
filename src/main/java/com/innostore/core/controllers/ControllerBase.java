package com.innostore.core.controllers;

import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;

abstract public class ControllerBase {
    public abstract String indexAction(HttpSession session, Model model);
}
