package com.innostore.core.mailer;

import com.innostore.core.common.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.nio.file.Path;
import java.util.*;

public class Mail {
    private static List<Path> attachments = new ArrayList<>();
    private static final Logger log = LoggerFactory.getLogger(Mail.class);

    public static boolean send(String to, String subject, String txtMessage, String htmlMessage) {
        boolean sended = false;
        Properties props = Settings.getSettings();

        Properties emailProps = new Properties();
        emailProps.put("mail.smtp.auth", props.getProperty("mail.smtp.auth"));
        emailProps.put("mail.smtp.starttls.enable", props.getProperty("mail.smtp.tls"));
        emailProps.put("mail.smtp.host", props.getProperty("mail.smtp.host"));
        emailProps.put("mail.smtp.port", props.getProperty("mail.smtp.port"));
        emailProps.put("mail.smtp.user", props.getProperty("mail.from"));
        emailProps.put("mail.debug", props.getProperty("mail.debug"));

        Session session = Session.getInstance(emailProps, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(props.getProperty("mail.smtp.login"), props.getProperty("mail.smtp.password"));
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(props.getProperty("mail.from")));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            message.setSubject(subject);
            Date date = new Date();

            // create mixed mail body with TXT and HTML
            MailBuilder mailBuilder = new MailBuilder();
            Multipart mpMixed = mailBuilder.build(txtMessage, htmlMessage, attachments);
            message.setContent(mpMixed,"UTF-8");
            message.setSentDate(date);

            log.debug("Sending mail to recipient list: {}", to);
            Transport.send(message);
            log.debug("Sending mail done.");
            sended = true;
        } catch (MessagingException e) {
            log.error(e.getMessage(), e.getCause());
            log.error("Mail error with properties: {}", Arrays.asList(emailProps));
        } finally {
            if(attachments.size() > 0)
                attachments.clear();
        }
        return sended;
    }

    public List<Path> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Path> attachments) {
        this.attachments = attachments;
    }
}
