package com.innostore.core.exceptions;

public class ConfirmDAOException extends Exception {
    public ConfirmDAOException(String s) {
        super(s);
    }

}
