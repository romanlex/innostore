package com.innostore.core.exceptions;

public class CategoryDAOException extends Exception {
    public CategoryDAOException(String s) {
        super(s);
    }

}
