package com.innostore.core.exceptions;

public class ProfileDAOException extends Exception {
    public ProfileDAOException(String s) {
        super(s);
    }

}
