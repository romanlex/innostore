package com.innostore.core.listeners;

import com.innostore.Main;
import com.innostore.core.Core;
import com.innostore.core.exceptions.CoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.LinkedHashMap;
import java.util.Map;

public class ApplicationLoadListener implements ServletContextListener {
    private static final Logger log = LoggerFactory.getLogger(ApplicationLoadListener.class);
    ServletContext context;



    @Override
    public void contextInitialized(ServletContextEvent contextEvent) {
//        final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(contextEvent.getServletContext());
//        final Properties props = (Properties)springContext.getBean("myProps");

        System.out.println("Context Created");
        try {
            Core core = new Core();
            core.load();
            ServletContext sc = contextEvent.getServletContext();
            sc.setAttribute("ctx", sc.getContextPath());

            Map<String, String> menu = new LinkedHashMap<>();
            menu.put("/", "<i class=\"fa fa-home\" aria-hidden=\"true\"></i> Главная");
            menu.put("/catalog", "<i class=\"fa fa-shopping-bag\" aria-hidden=\"true\"></i> Каталог");
            menu.put("/delivery", "<i class=\"fa fa-truck\" aria-hidden=\"true\"></i> Доставка");
            menu.put("/contacts", "<i class=\"fa fa-phone\" aria-hidden=\"true\"></i> Контакты");
            sc.setAttribute("menu", menu);


            Map<String, String> adminmenu = new LinkedHashMap<>();
            adminmenu.put("/", "<i class=\"fa fa-home\" aria-hidden=\"true\"></i> Главная");
            adminmenu.put("/catalog", "<i class=\"fa fa-shopping-bag\" aria-hidden=\"true\"></i> Каталог");
            adminmenu.put("/categories", "<i class=\"fa fa-database\" aria-hidden=\"true\"></i> Категории");
            adminmenu.put("/users", "<i class=\"fa fa-users\" aria-hidden=\"true\"></i> Пользователи");
            adminmenu.put("/orders", "<i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i> Заказы");
            sc.setAttribute("adminmenu", adminmenu);

        } catch (CoreException e) {
            log.error(e.getMessage(), Main.class);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {
        System.out.println("Context Destroyed");
    }
}
