package com.innostore.core.common;

import com.innostore.core.exceptions.CoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utils {

    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    /**
     * NOT UNIT TESTED Returns the URL (including query parameters) minus the scheme, host, and
     * context path.  This method probably be moved to a more general purpose
     * class.
     */
    public static String getRelativeUrl(HttpServletRequest request ) {
        String baseUrl = null;
        if ( ( request.getServerPort() == 80 ) ||
                ( request.getServerPort() == 443 ) )
            baseUrl =
                    request.getScheme() + "://" +
                            request.getServerName() +
                            request.getContextPath();
        else
            baseUrl =
                    request.getScheme() + "://" +
                            request.getServerName() + ":" + request.getServerPort() +
                            request.getContextPath();

        StringBuffer buf = request.getRequestURL();

        if ( request.getQueryString() != null ) {
            buf.append( "?" );
            buf.append( request.getQueryString() );
        }

        return buf.substring( baseUrl.length() );
    }

    /**
     * NOT UNIT TESTED Returns the base url (e.g, <tt>http://myhost:8080/myapp</tt>) suitable for
     * using in a base tag or building reliable urls.
     */
    public static String getBaseUrl( HttpServletRequest request ) {
        if ( ( request.getServerPort() == 80 ) ||
                ( request.getServerPort() == 443 ) )
            return request.getScheme() + "://" +
                    request.getServerName() +
                    request.getContextPath();
        else
            return request.getScheme() + "://" +
                    request.getServerName() + ":" + request.getServerPort() +
                    request.getContextPath();
    }


    /**
     * Return MD5 string
     * @param str get md5 hash string by string
     * @return String
     */
    public static String getMd5String(String str) throws CoreException {
        String code = null;
        try {
            byte[] bytesOfMessage = str.getBytes("UTF-8");
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] md5 = md.digest(bytesOfMessage);
            StringBuilder sb = new StringBuilder();
            for (byte aMd5 : md5) {
                sb.append(Integer.toHexString((aMd5 & 0xFF) | 0x100).substring(1, 3));
            }
            code = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(),e.getCause());
            throw new CoreException("Не получилось получить алгоритм шифрования. Обратитесь к администрации для решения проблемы.");
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(),e.getCause());
        }
        return code;
    }
}
