package com.innostore.app.models;

import com.innostore.core.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class OrderModel extends Model {

    private static final String table = "orders";
    private static Logger log = LoggerFactory.getLogger(OrderModel.class);

    public OrderModel() {
        setSource(table);
        String[] many = {
            "id",
            "OrderProductsModel",
            "orderId"
        };
        hasMany(many);
    }

}



