package com.innostore.app.models;

import com.innostore.app.enums.Category;
import com.innostore.app.repository.CategoriesRepository;
import com.innostore.app.services.impl.CategoriesServiceImpl;
import com.innostore.core.exceptions.CategoryDAOException;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

@Repository
public class CategoryModel extends Model implements CategoriesRepository {

    private static final String table = "categories";
    private static Logger log = LoggerFactory.getLogger(CategoryModel.class);

    public CategoryModel() {
        setSource(table);
    }

    @Override
    public ResultSet find() throws ModelException {
        return this._find();
    }

    @Override
    public ResultSet find(String condition) throws ModelException {
        return this._find(condition);
    }

    @Override
    public int deleteFirst(int id) throws ModelException {
        return this._deleteFirst(id);
    }

    @Override
    public long save(HashMap<String, Object> map) throws ModelException {
        return this._save(map);
    }

    @Override
    public Category getCategoryById(int id) throws CategoryDAOException {
        Category category = new Category();
        try {
            ResultSet rs = this._findFirst(id);
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            if(rows == 0)
                throw new CategoryDAOException("Категория с данным ID (" + id + ") не найдена");

            while (rs.next()) {
                CategoriesServiceImpl.bindToCategory(category, rs);
            }
        } catch (SQLException | ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new CategoryDAOException(e.getMessage());
        }
        return category;
    }
}
