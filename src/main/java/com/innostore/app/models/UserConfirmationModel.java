package com.innostore.app.models;

import com.innostore.app.enums.UserConfirmation;
import com.innostore.app.repository.UserConfirmationRepository;
import com.innostore.core.database.Database;
import com.innostore.core.exceptions.ConfirmDAOException;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

@Repository
public class UserConfirmationModel extends Model implements UserConfirmationRepository {

    private static final String table = "users_confirmations";
    private static Logger log = LoggerFactory.getLogger(UserConfirmationModel.class);

    private static final String FIND_CONFIRM_SQL = "SELECT * FROM " + table + " WHERE code=? LIMIT 1";

    public UserConfirmationModel() {
        setSource(table);
    }

    @PostConstruct
    public void userConfirmationModellInit(){
        log.info("Initialize UserConfirmationModel");
    }

    @Override
    public UserConfirmation findByCode(String code) throws ConfirmDAOException {
        UserConfirmation confirm = new UserConfirmation();
        Connection db = Database.pool.checkOut();
        try (PreparedStatement ps = db.prepareStatement(FIND_CONFIRM_SQL)) {
            ps.setString(1, code);
            ResultSet rs = ps.executeQuery();
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            if(rows == 0)
                throw new ConfirmDAOException("Ссылка для активации не действительна");

            while (rs.next()) {
                confirm.setId(rs.getInt("id"));
                confirm.setUserId(rs.getInt("userId"));
                confirm.setCode(rs.getString("code"));
                confirm.setDate(rs.getTimestamp("date"));
                confirm.setConfirmed(rs.getString("confirmed"));
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ConfirmDAOException(e.getMessage());
        }
        Database.pool.checkIn(db);
        return confirm;
    }

    @Override
    public long save(HashMap<String, Object> map) throws ModelException {
        return this._save(map);
    }
}



