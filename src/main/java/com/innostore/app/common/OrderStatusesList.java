package com.innostore.app.common;


import com.innostore.app.enums.OrderStatus;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "orderStatuses")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderStatusesList {
    @XmlElement(name = "status")
    private List<OrderStatus> orderStatuses = null;

    public List<OrderStatus> getOrderStatuses() {
        return orderStatuses;
    }

    public void setOrderStatuses(List<OrderStatus> orderStatuses) {
        this.orderStatuses = orderStatuses;
    }
}
