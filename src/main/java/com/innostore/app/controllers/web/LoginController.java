package com.innostore.app.controllers.web;

import com.innostore.app.enums.Role;
import com.innostore.app.enums.User;
import com.innostore.app.services.UserService;
import com.innostore.core.exceptions.UserDAOException;
import com.innostore.core.notification.AdminAuthentificatedNotification;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class LoginController {
    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value="/login", method = RequestMethod.GET)
    @Secured("ROLE_ANONYMOUS")
    public String indexAction(HttpSession session, Model model) {
        return "login";
    }

    @RequestMapping(value="/login", method = RequestMethod.POST)
    @Secured("ROLE_ANONYMOUS")
    public String indexPostAction(HttpServletRequest req,
                                  HttpSession session,
                                  Model model) {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        EmailValidator emailValidator = EmailValidator.getInstance();
        if(!emailValidator.isValid(email))
        {
            model.addAttribute("error", "Email указан не корректно");
            return "login";
        }

        if(password.equals("")) {
            model.addAttribute("error", "Укажите пароль");
            return "login";
        }

        try {
            User user = userService.authenticated(email, password);
            user.setPassword("");
            session.setAttribute("user", user);
            List<Role> roles = user.getAuthorities();
            if(roles.contains("ROLE_ADMIN") || roles.contains("ROLE_SUPERADMIN")) {
                Thread th = new Thread(() -> AdminAuthentificatedNotification.notifySuperAdmin(userService, user, req));
                th.start();
            }
            return "redirect:/";
        } catch ( UserDAOException e) {
            model.addAttribute("error", e.getMessage());
        }
        return "login";
    }
}
