package com.innostore.app.controllers.web;

import com.innostore.app.enums.User;
import com.innostore.app.enums.UserConfirmation;
import com.innostore.app.models.UserModel;
import com.innostore.app.services.ConfirmService;
import com.innostore.app.services.UserService;
import com.innostore.core.exceptions.ConfirmDAOException;
import com.innostore.core.exceptions.CoreException;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.UserDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;

@Controller
public class ConfirmController {
    private static final Logger log = LoggerFactory.getLogger(ConfirmService.class);

    private UserService userService;
    private ConfirmService confirmService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setConfirmService(ConfirmService confirmService) {
        this.confirmService = confirmService;
    }

    @RequestMapping(value="/confirm/{code}", method = RequestMethod.GET)
    public String indexAction(@PathVariable String code, HttpSession session, Model model) {
        if(code == null || code.equals(""))
        {
            model.addAttribute("error", "Не указан идентификатор подтверждения. Ссылка на подтверждение не действительна");
            return "forward:login";
        }

        try {
            UserConfirmation confirm = confirmService.findByCode(code);
            int userId = confirm.getUserId();

            if(confirm.getConfirmed().equals("Y"))
            {
                model.addAttribute("warning", "Ссылка уже была ранее активирована. Попробуйте авторизоваться с данными вашей учетной записи.");
                return "forward:login";
            }

            Date date = new Date();
            java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());

            confirm.setConfirmed("Y");
            confirm.setDate(timestamp);

            if( confirmService.save(confirm) > 0 ) {
                User user = userService.getUserById(userId);
                user.setSuspended("N");
                HashMap<String, Object> userDate = new HashMap<>();
                userDate.put("id", user.getId());
                userDate.put("suspended", user.getSuspended());
                UserModel users = new UserModel();
                if(users.save(userDate) > 0)
                {
                    model.addAttribute("success", "Ваш аккаунт успешно подтверждён. Вы можете авторизоваться с данными вашей учетной записи.");

                } else {
                    log.error("Cannot update user with confirmation");
                    model.addAttribute("error", "Не удалось обновить информацию пользователя. Попробуйте позднее или свяжитесь с администраторами ресурса.");
                }
            } else {
                log.error("Cannot save data of user confirmation");
                model.addAttribute("error", "Не удалось обновить информацию о подтверждении аккаунта. Попробуйте позднее или свяжитесь с администраторами ресурса.");
            }
        } catch (CoreException | ModelException | ConfirmDAOException | UserDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
        }
        return "login";
    }
}
