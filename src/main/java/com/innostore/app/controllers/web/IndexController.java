package com.innostore.app.controllers.web;

import com.innostore.app.enums.Product;
import com.innostore.app.models.CategoryModel;
import com.innostore.app.models.ProductModel;
import com.innostore.app.services.ProductService;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.ProductDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Controller
public class IndexController {
    private static final Logger log = LoggerFactory.getLogger(IndexController.class);
    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }


    @RequestMapping(value="/", method = RequestMethod.GET)
    public String indexAction(Model model) {
        ArrayList<Product> productList;
        try {
            productList = productService.find("popular = 'Y'");
            model.addAttribute("products", productList);
        } catch (ProductDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
        }
        return "index";
    }

    private void printAllCategories() {
        try {
            System.out.println("Get all categories:");
            CategoryModel categories = new CategoryModel();
            ResultSet result = categories._find();
            System.out.println("+-------+-------------------+--------------------------------");
            System.out.println("| id\t| name\t\t\t\t|  description");
            System.out.println("+-------+-------------------+--------------------------------");
            while (result.next()) {
                System.out.println("| " + result.getInt("id") + "\t|  " +
                        result.getString("name") + "\t\t|  " +
                        result.getString("description"));
                System.out.println("+-------+-------------------+--------------------------------");
            }

            int rowCount = result.last() ? result.getRow() : 0;
            System.out.printf("Found "  + rowCount + " rows\n\n");
        } catch (SQLException | ModelException e) {
            log.error("Cannot get categories: {}", e.getMessage());
        }
    }

    private void printAllProducts() {
        try {
            System.out.println("Get all products:");
            ProductModel products = new ProductModel();
            ResultSet result = products.find();
            System.out.println("+-------+---------------+------------+------------------+---------------+--------------");
            System.out.println("| id\t|  article\t\t| categoryId |  name\t\t\t|  vendor\t\t|  price");
            System.out.println("+-------+---------------+------------+------------------+---------------+--------------");
            while (result.next()) {
                System.out.println("| " + result.getInt("id") + "\t\t|  " +
                        result.getString("article") + "\t\t|  " +
                        result.getInt("categoryId") + "\t\t |  " +
                        result.getString("name") + "\t\t|  " +
                        result.getString("vendor") + "\t\t|  " +
                        result.getDouble("price"));
                System.out.println("+-------+---------------+------------+------------------+---------------+--------------");
            }

            int rowCount = result.last() ? result.getRow() : 0;
            System.out.println("Found "  + rowCount + " rows");

        } catch (SQLException | ModelException e) {
            log.error("Cannot get products: {}", e.getMessage());
        }
    }
}
