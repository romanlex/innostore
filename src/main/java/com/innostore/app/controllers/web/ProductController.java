package com.innostore.app.controllers.web;

import com.innostore.app.enums.Product;
import com.innostore.app.services.ProductService;
import com.innostore.core.exceptions.ProductDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@Controller
public class ProductController {
    private static final Logger log = LoggerFactory.getLogger(ProductController.class);

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(value="/product/{idString}", method = RequestMethod.GET)
    public String indexAction(@PathVariable String idString, HttpSession session, Model model) {
        if(idString == null || idString.equals(""))
        {
            model.addAttribute("error", "Укажите ID товара");
            return "forward:product";
        }

        int id = Integer.parseInt(idString);
        try {
            Product product = productService.getProductById(id);
            model.addAttribute("product", product);
        } catch ( ProductDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
            return "forward:product";
        }
        return "product";
    }

}
