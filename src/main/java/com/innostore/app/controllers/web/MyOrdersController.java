package com.innostore.app.controllers.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@Controller
public class MyOrdersController {

    @RequestMapping(value="/myorders", method = RequestMethod.GET)
    public String indexAction(HttpSession session, Model model) {
        return "myorders";
    }

}
