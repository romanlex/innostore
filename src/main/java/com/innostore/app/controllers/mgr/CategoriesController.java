package com.innostore.app.controllers.mgr;

import com.innostore.app.enums.Category;
import com.innostore.app.services.CategoriesService;
import com.innostore.core.exceptions.CategoryDAOException;
import com.innostore.core.exceptions.ModelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

@Controller
public class CategoriesController {

    private static final Logger log = LoggerFactory.getLogger(CategoriesController.class);

    private CategoriesService categoriesService;

    @Autowired
    public void setCategoriesService(CategoriesService categoriesService) {
        this.categoriesService = categoriesService;
    }

    @RequestMapping(value="/categories", method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public String indexAction(HttpSession session, Model model) {
        ArrayList<Category> categories;
        try {
            categories = categoriesService.find();
            model.addAttribute("categories", categories);
        } catch (CategoryDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
        }
        return "manager/mgr_categories";
    }

    /**
     * ДОБАВЛЕНИЕ ТОВАРА
     */
    @RequestMapping(value="/categories/add", method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public String addAction() {
        return "manager/mgr_category_add";
    }

    @RequestMapping(value="/categories/add", method = RequestMethod.POST)
    @Secured("ROLE_ADMIN")
    public String addPostAction(HttpServletRequest req, HttpSession session, Model model) throws UnsupportedEncodingException {
        req.setCharacterEncoding("UTF-8");
        try {
            if (categoriesService.save(req) > 0) {
                return "redirect:/categories";
            } else {
                model.addAttribute("error", "Произошла ошибка при добавлении категории. Ни одна запись в БД не была сохранена.");
                return "manager/mgr_category_add";
            }
        } catch (ModelException | CategoryDAOException e) {
            log.error(e.getMessage(),e.getCause());
            model.addAttribute("error", "Произошла ошибка при сохранении категории: " + e.getMessage());
            return "manager/mgr_category_add";
        }
    }


    /**
     * УДАЛЕНИЕ КАТЕГОРИИ
     */
    @RequestMapping(value={"/categories/delete/*", "/categories/delete/", "/categories/delete"}, method = RequestMethod.POST)
    @Secured("ROLE_ADMIN")
    public String deletePostAction(HttpServletRequest req, HttpSession session, Model model) {
        try {
            if (categoriesService.deleteCategory(req) > 0) {
                return "redirect:/categories";
            } else {
                model.addAttribute("error", "Категория с данным ID не найдена");
            }
        } catch (CategoryDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
        }
        return "manager/mgr_categories";
    }


    /**
     * РЕДАКТИРОВАНИЕ КАТЕГОРИИ
     */
    @RequestMapping(value={"/categories/edit/", "/categories/edit"} , method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public String editActionWrong(HttpSession session, Model model) {
        model.addAttribute("error", "Укажите ID категории");
        return "manager/mgr_categories";
    }

    @RequestMapping(value={"/categories/edit/{idString}"} , method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public String editAction(@PathVariable String idString, Model model) {
        if(idString == null || idString.equals(""))
        {
            model.addAttribute("error", "Укажите ID категории");
            return "manager/mgr_categories";
        }

        int id = Integer.parseInt(idString);

        try {
            Category category = categoriesService.getCategoryById(id);
            model.addAttribute("category", category);
        } catch (CategoryDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
            return "manager/mgr_categories";
        }
        return "manager/mgr_category_edit";
    }

    @RequestMapping(value={"/categories/edit/*", "/categories/edit/", "/categories/edit"}, method = RequestMethod.POST)
    @Secured("ROLE_ADMIN")
    public String editPostAction(HttpServletRequest req, HttpSession session, Model model) throws UnsupportedEncodingException {
        req.setCharacterEncoding("UTF-8");
        try {
            if (categoriesService.save(req) > 0) {
                Category updatedCategory = categoriesService.getCategoryById(Integer.parseInt(req.getParameter("id")));
                model.addAttribute("category", updatedCategory);
                model.addAttribute("success", "Информация о категории успешно обновлёна");
            } else {
                model.addAttribute("error", "Произошла ошибка при обновлении категории. Ни одна запись в БД не была обновлена.");
            }
        } catch (ModelException | CategoryDAOException e) {
            log.error(e.getMessage(),e.getCause());
            model.addAttribute("error", "Произошла ошибка при обновлении категории: " + e.getMessage());
        }
        return "manager/mgr_category_edit";
    }
}
