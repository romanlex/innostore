package com.innostore.app.controllers.mgr;

import com.innostore.core.common.Export;
import com.innostore.core.exceptions.CoreException;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.model.Model;
import com.innostore.core.model.ResultSetMapper;
import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ExportController {

    private static final boolean MULTI_THREAD_SUPPORT = true;
    private static ArrayList<Thread> threads = new ArrayList<>();
    private static final Logger log = LoggerFactory.getLogger(ExportController.class);

    /**
     * Index entry point for ExportController
     */
    public void indexAction() {
        log.debug("Exporting data from database");
        String[] tables = {
                "Product", "CategoriesModel", "User", "Order", "OrderStatus"
        };


        for (String table : tables) {
            exportTable(table);
        }

        for (Thread thread:
                threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        log.info("Export process finished.");
    }


    /**
     * This method get name of model class which required for marshaling to xml
     * @param table tablename for export
     */
    private static void exportTable(String table) {
        log.info("Load {} model", table);
        try {
            Object listObject = Class.forName("com.innostore.app.models." + table + "List").newInstance();
            Model model = (Model) Class.forName("com.innostore.app.models." + table).newInstance();

            Method hasRelationsMethod = model.getClass().getMethod("hasRelations");
            Boolean hasRelations = (Boolean) hasRelationsMethod.invoke(model);

            List<Object> relatedObjects = new ArrayList<>();
            ResultSetMapper<Object> resultSetRelationMapper = new ResultSetMapper<>();
            String[] relation = {};
            if(hasRelations) {
                relation = getRelations(table, model);
                ResultSet relationResult = getRelationModelResult(relation, table, model);

                /* Fucking code, spent two days
                 relation[0] - column in main model
                 relation[1] - table for relation
                 relation[2] - column in relation table
                 */
                Object relationObject = Class.forName("com.innostore.app.models." + relation[1]).newInstance();
                resultSetRelationMapper = new ResultSetMapper<>();
                relatedObjects = resultSetRelationMapper.mapRersultSetToObject(relationResult, relationObject.getClass(), relation);


                if(relatedObjects == null)
                    log.error("ResultSet of relation is empty. Please check if database table is empty");

            }

            // Get result main model
            ResultSet result = model._find();
            int rows = 0;

            if (result.last()) {
                rows = result.getRow();
                result.beforeFirst();
            }
            ResultSetMapper<Object> resultSetMapper = new ResultSetMapper<>();
            String[] rel = {};
            List<Object> objects = resultSetMapper.mapRersultSetToObject(result, model.getClass(), rel);

            if(relatedObjects != null && relatedObjects.size() > 0) {
                setRelatedObject(objects, relation, resultSetRelationMapper, model);
            }

            // Add to ModelList
            Class[] argTypes = { List.class };
            Method method = listObject.getClass().getMethod("set" + table, argTypes);
            method.invoke(listObject, objects);

            result.close();
            log.info("Found {} rows in {}", rows, table);

            if(objects.size() > 0) {
                log.info("Exporting collection...");
                startExportThread(listObject, model, table);
            }

        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            log.error("Method is not found: {}", e.getMessage() );
        } catch (ClassNotFoundException e) {
            log.error("Class not found when trying exporting: {}", e.getMessage() );
        } catch (SQLException e) {
            log.error("SQL ERROR: {}", e.getMessage() );
        } catch (CoreException | ModelException e) {
            log.error(e.getMessage(), e.getCause());
        }
    }

    /**
     * Method set relation objects to main object
     * @param objects List of object
     * @param relation Relation for object
     * @param resultSetRelationMapper ResultSetMapper for ResultSet
     * @param model Model of table
     */
    private static void setRelatedObject(List<Object> objects, String[] relation, ResultSetMapper<Object> resultSetRelationMapper, Model model) {
        try {
            Class[] argTypes = {List.class};

            Object listRelationObject = Class.forName("com.innostore.app.models." + relation[1] + "List").newInstance();
            Method setListRelationObjectMethod = listRelationObject.getClass().getMethod("set" + WordUtils.capitalize(relation[1]), argTypes);

            Class[] argTypes2 = {listRelationObject.getClass()};
            Method setRelation = model.getClass().getMethod("set" + WordUtils.capitalize(relation[1]) + "List", argTypes2);

            for (Object object : objects) {
                HashMap map = resultSetRelationMapper.getMap();

                Method methodGetId = object.getClass().getMethod("getId");
                Integer id = (Integer) methodGetId.invoke(object);
                for (Object o :
                        map.keySet()) {

                    // В коллекции найден элемент с id перебираемого объекта
                    if (map.get(o).equals(id)) {
                        setListRelationObjectMethod.invoke(listRelationObject, o);
                        log.info("Invoke method set" + WordUtils.capitalize(relation[1]) + " from " + listRelationObject.getClass());

                        setRelation.invoke(object, listRelationObject);
                        log.info("Invoke method set" + WordUtils.capitalize(relation[1]) + "List from " + model.getClass());
                    }
                }
            }
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            log.error("Error when trying setRelatedObject: {}", e.getMessage());
        } catch (ClassNotFoundException e) {
            log.error("Class not found in setRelatedObject: {}", e.getMessage());
        } catch (NoSuchMethodException e) {
            log.error("Method not found in setRelatedObject: {}", e.getMessage());
        }
    }

    /**
     * Get relations for model
     * @param table tablename
     * @param model Model of object
     * @return relation String[] array with key
     * @throws NoSuchMethodException method not found when trying set by reflection api
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    private static String[] getRelations(String table, Model model) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method getRelations = model.getClass().getMethod("getRelations");
        String[] relation = (String[]) getRelations.invoke(model);
        log.info("Model {} has relations {}", table, Arrays.asList(relation));
        return relation;
    }

    private static ResultSet getRelationModelResult(String[] relation, String table, Model model) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException, SQLException, ModelException, CoreException {
        log.debug("Load className com.innostore.app.models.{}", relation[1]);
        Model relationModel = (Model) Class.forName("com.innostore.app.models." + relation[1]).newInstance();

        ResultSet result = relationModel._find();

        int rows = 0;

        if (result.last()) {
            rows = result.getRow();
            result.beforeFirst();
        }
        log.info("Found {} rows in {}", rows, relation[1]);

        return  result;
    }

    /**
     * Started marshaling in threads
     * @param objects Object list for convert to xml
     * @param model Model of object
     * @param table Tablename
     */
    private static void startExportThread(Object objects, Model model, String table) {
        if(MULTI_THREAD_SUPPORT) {
            Thread thread = new Thread(() -> startExport(objects, model, table));
            thread.start();
            threads.add(thread);
        } else {
            startExport(objects, model, table);
        }
    }

    private static void startExport(Object objects, Model model, String table) {
        File file = new File("exported/" + table + ".xml");
        Export export = new Export(objects, model, table, file);
        export.start();
    }

}
