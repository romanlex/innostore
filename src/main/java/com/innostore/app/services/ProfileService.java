package com.innostore.app.services;

import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.ProfileDAOException;

import javax.servlet.http.HttpServletRequest;

public interface ProfileService {
    /**
     * Обработка запроса пользователя на сохранение/обновление продукта в базе
     * @param req HttpServletRequest запрос
     * @return количество затронутых строк в БД
     * @throws ProfileDAOException ошибка при поиске записи
     */
    long save(HttpServletRequest req) throws ProfileDAOException, ModelException;
}
