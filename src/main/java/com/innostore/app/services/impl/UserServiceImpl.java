package com.innostore.app.services.impl;

import com.innostore.app.enums.Role;
import com.innostore.app.enums.User;
import com.innostore.app.repository.UserRepository;
import com.innostore.app.services.UserService;
import com.innostore.core.common.Settings;
import com.innostore.core.common.Utils;
import com.innostore.core.exceptions.CoreException;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.UserDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {
    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    private UserRepository model;

    @Autowired
    public void setModel(UserRepository model) {
        this.model = model;
    }

    @Override
    public User authenticated(String email, String password) throws UserDAOException {
        User user = model.getUserByLogin(email);

        if(user.getId() == 0) {
            throw new UserDAOException("Пользователь с данным email не найден");
        }

        if(user.getBanned().equals("Y")) {
            log.info("Попытка авторизации заблокированного пользователя {} (#{})", user.getEmail(), user.getId());
            throw new UserDAOException("Ваш аккаунт заблокирован. Обратитесь к администрации для получения информации по разблокировке аккаунта.");
        }

        if(user.getSuspended().equals("Y") && Integer.parseInt(Settings.getSettings().getProperty("registration.confirm.email")) == 1) {
            log.info("Попытка авторизации неподтвержденного пользователя {} (#{})", user.getEmail(), user.getId());
            throw new UserDAOException("Ваш аккаунт не подтверждён. Активируйте ваш аккаунт пройдя по ссылке, которую вы получили ранее в письме.");
        }

        String userPost = null;
        try {
            String hash = Utils.getMd5String(user.getEmail() + password);
            userPost = Utils.getMd5String(Settings.getSettings().getProperty("salt") + hash);
        } catch (CoreException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException("Не удалось получить хэш данных");
        }

        if(!userPost.equals(user.getPassword()))
            throw new UserDAOException("Пароль указан не верно");

        return user;
    }

    @Override
    public long join(String email, String password, String firstname) throws UserDAOException, ModelException, CoreException {
        if(model.userExist(email))
            throw new UserDAOException("Пользователь с таким email существует");
        HashMap<String, Object> data = new HashMap<>();

        String hash = Utils.getMd5String(email + password);
        String md5Hash = Utils.getMd5String(Settings.getSettings().getProperty("salt") + hash);
        data.put("email", email);
        data.put("password", md5Hash);
        data.put("firstname", firstname);

        return model.save(data);
    }

    @Override
    public boolean userExist(String email) throws UserDAOException {
        return model.userExist(email);
    }

    @Override
    public User getUserById(int id) throws UserDAOException {
        return model.getUserById(id);
    }

    @Override
    public ArrayList<User> getUsersByRole(String role) throws UserDAOException {
        return model.getUsersByRole(role);
    }

    @Override
    public User getUserByLogin(String email) throws UserDAOException {
        return model.getUserByLogin(email);
    }

    @Override
    public int deleteUser(HttpServletRequest req) throws UserDAOException {
        if(req.getParameter("id").equals(""))
            throw new UserDAOException("Не получен ID юзера");

        int id = Integer.parseInt(req.getParameter("id"));

        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(currentUser.getId() == id)
            throw new UserDAOException("Вы в своём уме? Вы не можете удалить сами себя!");

        int deleted = 0;
        try {
            User user = getUserById(id);
            List<Role> roles = user.getAuthorities();
            if(roles.contains("ROLE_SUPERADMIN")) {
                log.info("User {} trying delete Super Admin account", currentUser.getEmail());
                throw new UserDAOException("Невозможно удалить супер админа.");
            }

            deleted = model.deleteFirst(user.getId());

        } catch (ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        return deleted;
    }

    @Override
    public long save(HttpServletRequest req) throws UserDAOException, ModelException {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int userId = currentUser.getId();

        HashMap<String, Object> data = new HashMap<>();

        for (Object key:
                req.getParameterMap().keySet()) {
            String _key = key.toString();
            String[] value = req.getParameterMap().get(key);
            data.put(_key, value[0]);
        }

        if(data.get("suspended") != null)
            data.put("suspended", "N");

        if(data.get("banned") != null)
            data.put("banned", "Y");

        data.putIfAbsent("suspended", "Y");
        data.putIfAbsent("banned", "N");

        List<Role> roles = currentUser.getAuthorities();

        if(req.isUserInRole("ROLE_USER"))
            throw new UserDAOException("У вас нет прав на добавление/изменение пользователей в системе");

        return model.save(data);
    }

    @Override
    public ArrayList<User> find() throws UserDAOException {
        ArrayList<User> userList = new ArrayList<>();
        try {
            ResultSet rs = model.find();
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            while (rs.next()) {
                bindToUserList(userList, rs);
            }
        } catch (SQLException | ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        return userList;
    }

    @Override
    public ArrayList<User> find(String condition) throws UserDAOException {
        ArrayList<User> users = new ArrayList<>();
        try {
            ResultSet rs = model.find(condition);
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            if(rows == 0)
                throw new UserDAOException("Пользователи в базе данных не найдены");

            while (rs.next()) {
                bindToUserList(users, rs);
            }

        } catch (SQLException | ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        return users;
    }


    public static void bindToUser(User user, ResultSet result) throws SQLException {
        user.setId(result.getInt("id"));
        user.setEmail(result.getString("email"));
        user.setPhone(result.getString("phone"));
        user.setPassword(result.getString("password"));
        user.setFirstname(result.getString("firstname"));
        user.setLastname(result.getString("lastname"));
        user.setGender(result.getInt("gender"));
        user.setCountry(result.getString("country"));
        user.setRegion(result.getString("region"));
        user.setCity(result.getString("city"));
        user.setAvatar(result.getString("avatar"));
        user.setPosition(result.getString("position"));
        user.setScope(result.getString("scope"));
        String _roles = result.getString("roles");
        List<Role> roles = new ArrayList<>();
        if(_roles.contains(",")){
            String[] manyroles = _roles.split(",");
            for (String _role:
                 manyroles) {
                Role role = new Role();
                role.setUserId(result.getInt("id"));
                role.setName(_role);
                roles.add(role);
            }
        } else {
            Role role = new Role();
            if(_roles.equals(""))
                role.setName("ROLE_USER");
            else
                role.setName(_roles);
            roles.add(role);
        }
        user.setAuthorities(roles);
        user.setBanned(result.getString("banned"));
        user.setSuspended(result.getString("suspended"));
    }

    public static void bindToUserList(ArrayList<User> userList, ResultSet result) throws SQLException {
        User user = new User();
        bindToUser(user, result);
        userList.add(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            return model.getUserByLogin(username);
        } catch (UserDAOException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
    }
}
