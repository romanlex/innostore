package com.innostore.app.services.impl;

import com.innostore.app.repository.UserRepository;
import com.innostore.core.exceptions.UserDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserService implements UserDetailsService {
    private static final Logger log = LoggerFactory.getLogger(CustomUserService.class);
    private UserRepository model;

    @Autowired
    public void setModel(UserRepository model) {
        this.model = model;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            return model.getUserByLogin(username);
        } catch (UserDAOException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
    }
}
