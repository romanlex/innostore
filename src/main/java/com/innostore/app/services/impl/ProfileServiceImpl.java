package com.innostore.app.services.impl;

import com.innostore.app.enums.User;
import com.innostore.app.repository.UserRepository;
import com.innostore.app.services.ProfileService;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.ProfileDAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@Service
public class ProfileServiceImpl implements ProfileService {
    private UserRepository model;

    @Autowired
    public void setModel(UserRepository model) {
        this.model = model;
    }

    @Override
    public long save(HttpServletRequest req) throws ProfileDAOException, ModelException {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int userId = currentUser.getId();

        if(req.getParameter("firstname") == null || req.getParameter("firstname").equals(""))
            throw new ProfileDAOException("Укажите ваше имя");

        if(req.getParameter("lastname") == null || req.getParameter("lastname").equals(""))
            throw new ProfileDAOException("Укажите вашу фамилию");

        if(req.getParameter("phone") == null || req.getParameter("phone").equals(""))
            throw new ProfileDAOException("Укажите ваш номер телефона");

        HashMap<String, Object> data = new HashMap<>();
        for (Object key:
                req.getParameterMap().keySet()) {
            String _key = key.toString();
            String[] value = (String[]) req.getParameterMap().get(key);
            data.put(_key, value[0]);
        }
        data.put("id", userId);


        return model.save(data);
    }
}
