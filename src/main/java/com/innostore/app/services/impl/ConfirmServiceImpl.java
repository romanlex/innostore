package com.innostore.app.services.impl;

import com.innostore.app.enums.UserConfirmation;
import com.innostore.app.repository.UserConfirmationRepository;
import com.innostore.app.services.ConfirmService;
import com.innostore.core.exceptions.ConfirmDAOException;
import com.innostore.core.exceptions.CoreException;
import com.innostore.core.exceptions.ModelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class ConfirmServiceImpl implements ConfirmService {
    private static final Logger log = LoggerFactory.getLogger(ConfirmServiceImpl.class);
    private UserConfirmationRepository model;

    @Autowired
    public void setModel(UserConfirmationRepository model) {
        this.model = model;
    }

    public UserConfirmation findByCode(String code) throws ConfirmDAOException, ModelException, CoreException {
        return model.findByCode(code);
    }

    @Override
    public long save(UserConfirmation confirm) throws ConfirmDAOException, ModelException {
        HashMap<String, Object> data = new HashMap<>();
        data.put("id", confirm.getId());
        data.put("date", confirm.getDate());
        data.put("confirmed", confirm.getConfirmed());

        return model.save(data);
    }
}
