package com.innostore.app.services;

import com.innostore.app.enums.Product;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.ProductDAOException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public interface ProductService {
    /**
     * Получает список всех продуктов из базы данных
     * @return список продуктов
     * @throws ProductDAOException ошибка при поиске записи
     */
    ArrayList<Product> find() throws ProductDAOException;

    /**
     * Получает список продуктов по условию condition из базы данных
     * @param condition условия выборки из базы данных
     * @return список продуктов
     * @throws ProductDAOException ошибка при поиске записи
     */
    ArrayList<Product> find(String condition) throws ProductDAOException;

    /**
     * Получает продукт по ID из базы данных
     * @param id ID в БД
     * @return объект продукта
     * @throws ProductDAOException ошибка при поиске записи
     */
    Product getProductById(int id) throws ProductDAOException;

    /**
     * Удаление продукта из БД
     * @param req HttpServletRequest запрос
     * @return количество затронутых строк в БД
     * @throws ProductDAOException ошибка при поиске записи
     */
    int deleteProduct(HttpServletRequest req) throws ProductDAOException;

    /**
     * Обработка запроса пользователя на сохранение/обновление продукта в базе
     * @param req HttpServletRequest запрос
     * @return количество затронутых строк в БД
     * @throws ProductDAOException ошибка при поиске записи
     */
    long save(HttpServletRequest req) throws ProductDAOException, ModelException;
}
