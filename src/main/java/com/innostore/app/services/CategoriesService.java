package com.innostore.app.services;

import com.innostore.app.enums.Category;
import com.innostore.core.exceptions.CategoryDAOException;
import com.innostore.core.exceptions.ModelException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public interface CategoriesService {
    /**
     * Получает список всех категорий из базы данных
     * @return список продуктов
     * @throws CategoryDAOException ошибка при поиске записи
     */
    ArrayList<Category> find() throws CategoryDAOException;

    /**
     * Получает список категорий по условию condition из базы данных
     * @param condition условия выборки из базы данных
     * @return список продуктов
     * @throws CategoryDAOException ошибка при поиске записи
     */
    ArrayList<Category> find(String condition) throws CategoryDAOException;

    /**
     * Получает категорию по ID из базы данных
     * @param id ID в БД
     * @return объект продукта
     * @throws CategoryDAOException ошибка при поиске записи
     */
    Category getCategoryById(int id) throws CategoryDAOException;

    /**
     * Удаление категории из БД
     * @param req HttpServletRequest запрос
     * @return количество затронутых строк в БД
     * @throws CategoryDAOException ошибка при поиске записи
     */
    int deleteCategory(HttpServletRequest req) throws CategoryDAOException;

    /**
     * Обработка запроса пользователя на сохранение/обновление категории в базе
     * @param req HttpServletRequest запрос
     * @return количество затронутых строк в БД
     * @throws CategoryDAOException ошибка при поиске записи
     */
    long save(HttpServletRequest req) throws CategoryDAOException, ModelException;
}
