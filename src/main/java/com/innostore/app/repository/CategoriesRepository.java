package com.innostore.app.repository;

import com.innostore.app.enums.Category;
import com.innostore.core.exceptions.CategoryDAOException;
import com.innostore.core.exceptions.ModelException;

import java.sql.ResultSet;
import java.util.HashMap;

public interface CategoriesRepository {
    /**
     * Получает список всех продуктов из базы данных
     * @return ResultSet
     * @throws ModelException ошибка при поиске записи
     */
    ResultSet find() throws ModelException;

    /**
     * Получает список продуктов по условию condition из базы данных
     * @param condition условия выборки из базы данных
     * @return ResultSet
     * @throws ModelException ошибка при поиске записи
     */
    ResultSet find(String condition) throws ModelException;

    /**
     * Удаление продукта из БД
     * @param id id
     * @return количество затронутых строк в БД
     * @throws ModelException ошибка при запросе в базу
     */
    int deleteFirst(int id) throws ModelException;

    /**
     * Обработка запроса пользователя на сохранение/обновление продукта в базе
     * @param map data
     * @return количество затронутых строк в БД
     * @throws ModelException ошибка при поиске записи
     */
    long save(HashMap<String, Object> map) throws ModelException;

    /**
     * Получает продукт по ID из базы данных
     * @param id ID в БД
     * @return объект продукта
     * @throws CategoryDAOException ошибка при поиске записи
     */
    Category getCategoryById(int id) throws CategoryDAOException;


}
