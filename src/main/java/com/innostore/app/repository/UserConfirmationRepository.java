package com.innostore.app.repository;

import com.innostore.app.enums.UserConfirmation;
import com.innostore.core.exceptions.ConfirmDAOException;
import com.innostore.core.exceptions.ModelException;

import java.util.HashMap;

public interface UserConfirmationRepository {
    /**
     * Получение конфирма по коду
     * @param code код конфирма
     * @return объект конфирма
     * @throws ConfirmDAOException ошибка при запросе
     */
    UserConfirmation findByCode(String code) throws ConfirmDAOException;

    long save(HashMap<String, Object> map) throws ModelException;
}
