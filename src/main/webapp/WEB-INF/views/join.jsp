<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - Регистрация</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper login__wrapper">
        <div class="login" id="join">
            <div class="login__logo">
                <i class="fa fa-superpowers" aria-hidden="true"></i> InnoStore
            </div>
            <div class="login__title">Регистрация</div>

            <c:if test="${requestScope.error != null}" >
                <div class="alert callout">
                    <p>
                        ${requestScope.error}
                    </p>
                </div>
            </c:if>

            <div class="login__form">
                <form action="/join" method="post">
                    <div class="form-group">
                        <input type="text" name="email" placeholder="Email" value="${param.email}">
                    </div>
                    <div class="form-group">
                        <input type="text" name="firstname" placeholder="Ваше имя" value="${param.firstname}">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" placeholder="Пароль" value="${param.password}">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password2" placeholder="Повторите пароль" value="${param.password2}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="button success">Присоединиться</button>
                        <a href="/login">У меня уже есть аккаунт</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>
<script src="/js/join.min.js"></script>

</body>
</html>