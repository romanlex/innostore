<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - Авторизация</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper login__wrapper">
        <div class="login" id="login">
            <div class="login__logo">
                <i class="fa fa-superpowers" aria-hidden="true"></i> InnoStore
            </div>
            <div class="login__title">Авторизация</div>

            <c:if test="${requestScope.success != null}" >
                <div class="success callout">
                    <p>
                        ${requestScope.success}
                    </p>
                </div>
            </c:if>

            <c:if test="${requestScope.error != null}" >
                <div class="alert callout">
                    <p>
                        ${requestScope.error}
                    </p>
                </div>
            </c:if>

            <c:if test="${requestScope.info != null}" >
                <div class="primary callout">
                    <p>
                            ${requestScope.info}
                    </p>
                </div>
            </c:if>

            <c:if test="${requestScope.warning != null}" >
                <div class="warning callout">
                    <p>
                        ${requestScope.warning}
                    </p>
                </div>
            </c:if>

            <div class="login__form">
                <form action="/j_spring_security_check" method="post">
                    <div class="form-group">
                        <input type="text" name="email" placeholder="Логин / Email" value="${param.email}">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" placeholder="Пароль">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="button">Войти</button>
                        <a href="/join">Зарегистрироваться</a>
                    </div>
                </form>
                <br><br>
                Demo:<br>
                SuperAdmin: r.olin.stc@innopolis.ru | 123123 | <button type="button" data-type="insert" data-insert-email="r.olin.stc@innopolis.ru" data-insert-password="123123">insert</button><br>
                Admin: lex-online@mail.ru | 123123 | <button type="button" data-type="insert" data-insert-email="lex-online@mail.ru" data-insert-password="123123">insert</button><br>
                User: user@mail.ru | 123123 | <button type="button" data-type="insert" data-insert-email="user@mail.ru" data-insert-password="123123">insert</button><br>
            </div>
        </div>
    </div>
</section>

<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>
<script src="/js/join.min.js"></script>

</body>
</html>