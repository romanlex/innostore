<ul>
    <li><a href="/profile">Профиль</a></li>
    <c:if test="${sessionScope.user.role != 'Admin' && sessionScope.user.role != 'SuperAdmin'}">
        <li><a href="/myorders">Мои заказы</a></li>
    </c:if>
    <li><a href="/logout">Выйти <i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
</ul>