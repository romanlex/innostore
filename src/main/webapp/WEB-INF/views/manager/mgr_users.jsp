<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.innostore.app.enums.User" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - Пользователи системы</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper">
        <%@include file="../includes/header.jsp"%>

        <div class="content-wrapper container">
            <%@include file="../includes/sidebar.jsp"%>
            <section class="content" id="content">
                <div class="page users">
                    <c:if test="${requestScope.success != null}" >
                        <div class="success callout">
                            <p>
                                ${requestScope.success}
                            </p>
                        </div>
                    </c:if>

                    <c:if test="${requestScope.error != null}" >
                        <div class="alert callout">
                            <p>
                                ${requestScope.error}
                            </p>
                        </div>
                    </c:if>

                    <c:if test="${requestScope.error == null}" >
                        <div class="x_panel">
                        <div class="x_title">
                            <h3>Список пользователей</h3>
                        </div>
                        <div class="x_content">
                            <c:if test="${not empty users}">
                                <table class="table unstriped" cellpadding="0" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th width="40">#</th>
                                        <th width="80">Email</th>
                                        <th>Номер телефона</th>
                                        <th>Имя</th>
                                        <th>Пол</th>
                                        <th>Забанен</th>
                                        <th>Подтверждён</th>
                                        <th>Роль</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${users}" var="user">
                                            <tr id="userId_${user.id}">
                                                <td>${user.id}</td>
                                                <td>
                                                    <a href="mailto:${user.email}?subject=Здравствуйте, ${user.firstname}">${user.email}</a>
                                                </td>
                                                <td>
                                                    ${user.phone}
                                                </td>
                                                <td>
                                                    ${user.firstname}
                                                </td>
                                                <td>
                                                    ${user.gender == 1 ? 'Мужской' : 'Женский'}
                                                </td>
                                                <td>
                                                    <span class="label ${user.banned == 'Y' ? 'alert' : 'success'}">${user.banned == 'Y' ? 'Да' : 'Нет'}</span>
                                                </td>
                                                <td>
                                                    <span class="label ${user.suspended == 'Y' ? 'alert' : 'success'}">${user.suspended == 'Y' ? 'Нет' : 'Да'}</span>
                                                </td>
                                                <td>
                                                    <span class="label ${user.role == 'Admin' ? 'warning' : (user.role == 'SuperAdmin' ? 'alert' : 'success' )}">${user.role}</span>
                                                </td>
                                                <td>
                                                    <div class="actions">
                                                        <c:if test="${(sessionScope.user.role == 'Admin' || sessionScope.user.role == 'SuperAdmin') && user.role == 'User'}">
                                                            <a href="/orders/user/${user.id}" title="Просмотреть заказы пользователя" class="hollow button tiny"><i class="fa fa-credit-card" aria-hidden="true"></i></a>
                                                        </c:if>
                                                        <c:if test="${(sessionScope.user.role == 'Admin' && sessionScope.user.id != user.id && user.role != 'SuperAdmin')}">
                                                            <a href="/users/edit/${user.id}" title="Редактировать" class="hollow button tiny"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                        </c:if>
                                                        <c:if test="${(sessionScope.user.role == 'SuperAdmin' && sessionScope.user.id != user.id)}">
                                                            <a href="/users/edit/${user.id}" title="Редактировать" class="hollow button tiny"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                            <form action="/users/delete/${user.id}" method="post">
                                                                <input type="hidden" name="id" value="${user.id}">
                                                                <button type="submit" data-type="delete" title="Удалить" class="hollow button tiny"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                            </form>
                                                        </c:if>
                                                    </div>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                     </tbody>
                                </table>
                            </c:if>
                            <c:if test="${empty users}">
                                <div class="secondary callout" style="margin-left: 10px; margin-right: 10px;">
                                    К сожалению еще нет ни одного зарегистрированного юзера.
                                </div>
                            </c:if>

                        </div>
                    </div>
                    </c:if>
                </div>
            </section>
        </div>
    </div>
</section>

<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>
<c:if test="${(sessionScope.user.role == 'Admin' || sessionScope.user.role == 'SuperAdmin')}">
    <script src="/js/mgr.min.js"></script>
</c:if>
</body>
</html>