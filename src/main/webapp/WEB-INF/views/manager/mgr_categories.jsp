<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.innostore.app.enums.Product" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - категории товаров</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper">
        <%@include file="../includes/header.jsp"%>

        <div class="content-wrapper container">
            <%@include file="../includes/sidebar.jsp"%>
            <section class="content" id="content">
                <div class="page categories">
                    <c:if test="${requestScope.success != null}" >
                        <div class="success callout">
                            <p>
                                ${requestScope.success}
                            </p>
                        </div>
                    </c:if>

                    <c:if test="${requestScope.error != null}" >
                        <div class="alert callout">
                            <p>
                                ${requestScope.error}
                            </p>
                        </div>
                    </c:if>

                    <c:if test="${requestScope.error == null}" >
                        <div class="x_panel">
                        <div class="x_title">
                            <h3>Категории товаров</h3>
                            <div class="x_title__actions">
                                <a href="/categories/add" class="button hollow">Добавить категорию</a>
                            </div>
                        </div>
                        <div class="x_content">
                            <div class="categories__category--table">
                                <c:if test="${not empty categories}">
                                    <table class="table unstriped" cellpadding="0" cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th width="40">#</th>
                                            <th>Имя</th>
                                            <th>Описание</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${categories}" var="category">
                                                <tr id="productId_${category.id}" class="category">
                                                    <td>${category.id}</td>
                                                    <td>
                                                        ${category.name}
                                                    </td>
                                                    <td>
                                                        ${category.description}
                                                    </td>
                                                    <td>
                                                        <div class="actions">
                                                            <a href="/categories/edit/${category.id}" title="Редактировать" class="hollow button tiny"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                            <form action="/categories/delete/${category.id}" method="post">
                                                                <input type="hidden" name="id" value="${category.id}">
                                                                <button type="submit" data-type="delete" title="Удалить" class="hollow button tiny"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                         </tbody>
                                    </table>
                                </c:if>
                                <c:if test="${empty categories}">
                                    <div class="secondary callout" style="margin-left: 10px; margin-right: 10px;">
                                        К сожалению не найдено ни одной категории
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    </c:if>
                </div>
            </section>
        </div>
    </div>
</section>

<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>
<c:if test="${(sessionScope.user.role == 'Admin' || sessionScope.user.role == 'SuperAdmin')}">
    <script src="/js/mgr.min.js"></script>
</c:if>
</body>
</html>